ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.2.0 (2023-10-31)
------------------

* Drop support for python 3.7.
* Add support for python 3.11 and 3.12.
* Update repo config (#2).
* Use setuptools-grpc (#3).
* Replace UUID with UID (#4).
* Update project setup.

1.1.1 (2022-03-30)
------------------

* Drop support for python 3.5 and 3.6.
* Add support for python 3.10.
* Add cmake support for C++ (#1).
* Update project setup.

1.1.0 (2021-07-28)
------------------

* Add write API.
* Fix protobuf dependencies.
* Rename CHANGELOG.
* Update project setup.

1.0.0 (2020-06-16)
------------------

Initial version.

* Add read-only access to files.
